# S-MOL
# created by unless games
# for 38th Ludum Dare with theme "A Small World"
# hours of hacks, dont read just play

Synth = null
init = () ->
  freq = (b, n) ->
    a = 1.059463094359
    b * Math.pow(a, n)

  palette = [
    "rgb(77, 155, 130)"
    ,"rgb(173, 59, 55)"
    ,"rgb(229, 139, 71)"
    ,"rgb(232, 175, 76)"
    ,"rgb(113, 72, 122)"
  ]
  finished = false
  notenames = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]
  voice = (rl) -> 
    f = T("sin", { freq:0, mul:0.05})
    osc = T("perc", {r:rl, freq:0, mul:1}, f)
    # osc.play()
    { osc : osc, f : f}


  major = [0, 2, 2, 1, 2, 2, 2, 1]
  minor = [0, 2, 1, 2, 2, 1, 2, 2]

  currentlevel = null

  synth = (vs, rl) ->
    voices = []
    for i in [0..vs-1]
      voices.push (voice(rl))
    s = { voices : voices }
    s.playnote = (n,i) ->
      s.voices[i].f.freq.value = Notetable[ n ]
      s.voices[i].osc.bang().play()
    s
      
  Synth = synth(16, 200)

  Arp = 
    tick : 0
    step : 0
    bpm : 360
    notes : []
    length : 0
    play : () ->
      if currentlevel.arpevent? then currentlevel.arpevent(Arp.step)
      if Arp.notes.length > 0
        for i in [0..Arp.notes.length - 1]
          if Arp.step is Arp.notes[i].p then Synth.playnote(Arp.notes[i].f, i)
        Arp.step = (Arp.step + 1) % Arp.length
    setbpm : (b) ->
      Arp.bpm = b
      window.clearInterval(Arp.tick)
      Arp.tick = window.setInterval(Arp.play, 60000 / b)
    init : ( b, ns, l) ->
      Arp.step = 0
      Arp.length = l
      Arp.setbpm(b)
      Arp.notes = ns



  newnotetable = (octaves) ->
    n = {}
    ii = 0
    for i in [0..octaves - 1]
      for nm in [0..11]
        n[notenames[nm]+i] = freq(  130.813, ii)
        ii++
    n

  newnotetablejust = (octaves) ->
    n = {}
    ii = 0
    ratios = [1, 25/24, 9/8 , 6/5, 5/4, 4/3, 45/32, 3/2, 8/5, 5/3, 9/5, 15/8]
    for i in [1..octaves]
      for nm in [0..11]
        n[notenames[nm] + (i - 1)] = i*(327.03*0.5) * ratios[nm]
        ii++
    n

  Notetable = newnotetable(4)

  lerp = (a, b, t) -> a*(1-t)+b*t
  ilerp = (a, b, p) -> (p - a) / (b - a)

  reverse = (str) ->
    r = ""
    for i in [str.length-1..0]
      r += str[i]
    r

  load = (lvl) ->
    $("#level").remove()
    $("#main").html $("<div>", {id: "level", html:lvl.insta()})
    Synth = synth(16, lvl.releaselength)
    Arp.init(lvl.bpm, lvl.notes(), lvl.arplength)

    currentlevel = lvl

  drones = T("sin", {mul:0.5, freq : Notetable["E0"]})
  droneosc = T("saw", {mul:0.5, freq : Notetable["E0"]}, drones)
  drone = T("lpf", {mul:0.15, res:0.5, freq : Notetable["E0"]}, droneosc)


  ui = 
    popup : (t, lvl) ->
      $("#ui").html t
      $("#ui").css("display", "block")
      $("#ui").animate({opacity:1}, 400, () -> $("#ui").animate({opacity:0}, 400, () -> $("#ui").css("display", "none")))
    end : () ->
      if not finished
        ls = []
        for i in [1..5]
          ls.push $("<button>", {html:"#{i}", click: (e)->loadlevel parseInt(e.target.innerHTML) })
        levels = $("<div>", { id: "levels", html: ls, style:"display:inline-block"})
        c = $("<div>", {id:"uibottom", html:["thanks for playing!", levels]})

        ug = $("<div>",{ style:"display:inline-block", html:"s-mol by unless games"})
        changespeed = (l, b) ->
          currentlevel.releaselength *= l
          currentlevel.bpm *= b
          Synth = synth(16, currentlevel.releaselength)
          Arp.setbpm(currentlevel.bpm)
        sm = $("<button>", {id:"speedminus", html: "-", click:()->changespeed(1.2, 0.8333)})
        sp = $("<button>", {id:"speedplus", html: "+", click:()->changespeed(0.8333, 1.2)})
        speeds = $("<div>", {id:"speeds", html:[sm, sp], style:"display : inline-block"})
        cc = $("<div>", {id : "uititle", html:[ug, speeds]})
        $("#ui").html c
        $("#ui").append cc
        $("#uititle").animate({"top": "+=40px"}, 1500, ()-> $("#uititle").animate({"top": "-=40px", opacity: 1}, 1000))
        $("#uibottom").animate({"top": "-=40px"}, 100, ()-> $("#uibottom").animate({"top": "+=40px", opacity: 1}, 1000))
        finished = true
        "congrats!"

  fifth = 
    arplength : 6
    releaselength : 800
    noteindexes : [0, 7, 6, 5, 3]
    bpm : 64
    scale : ["E0", "F#0", "G0", "A0", "B0", "C1", "D1",    "E1", "F#1", "G1", "A1", "B1", "C2", "D2",    "E2", "F#2", "G2", "A2", "B2", "C3",  "D3" ]
    ap : 0
    bp : 2
    cp : 11
    notes : () -> 
      r = []
      ii = 0
      for i in fifth.noteindexes
        r.push {f:fifth.scale[i + fifth.ap], p : ii}
        r.push {f:fifth.scale[(i + fifth.ap + fifth.bp) % fifth.scale.length], p : ii}
        r.push {f:fifth.scale[(i + fifth.ap + fifth.cp) % fifth.scale.length], p : ii}
        ii++
      r
    selecting : "accord"
    selected : "ap"
    selectednote : "f0"
    noteactions : 0
    chordactions : 0

    insta : () ->
      back = $("<div>", {id:"back"})
      a = $("<div>", {id:"ap"})
      b = $("<div>", {id:"bp"})
      c = $("<div>", {id:"cp"})

      a.css { top :(300 - (13*fifth.ap) - 13) + "px", opacity : 0.7}
      b.css { top :(300 - (13*fifth.bp) - 13) + "px", opacity : 0.7}
      c.css { top :(300 - (13*fifth.cp) - 13) + "px", opacity : 0.7}

      ns = $("<div>", {id:"notes"})
      for i in [0..4]
        n = $("<div>", {id : "f"+i, class: "note"})
        n.css {position:"absolute", left:i * (300 / 5), top: (8 - fifth.noteindexes[i]) * (300/8)-(300/8), width: (300/5) + "px", height: (300/8) + "px", "background-color":"#aaa" }
        n.click((e)->
            if fifth.selecting is "accord" then $("##{fifth.selected}").css("opacity", 0.7)
            $("##{fifth.selectednote}").removeClass "selected"
            fifth.selectednote = e.target.id
            fifth.selecting = "note"
            $(e.target).addClass "selected"
            )
        ns.append n

      setaccord = (e, y) ->
        p = Math.floor((y / 300) * 23)
        $("##{e}").css {top:  p * 13}
        fifth[e] = 22 - p
        Arp.notes = fifth.notes()
      
      setnote = (e, y) ->
        p = Math.floor((y / 300) * 8)
        $("##{e}").css {top:  p * (300/8)}
        fifth.noteindexes[parseInt(e.substr(1,1), 10)] = 7 - p
        Arp.notes = fifth.notes()
      
      moveto = (e) ->
        if fifth.selecting is "accord"
          setaccord(fifth.selected, e.offsetY) 
          fifth.chordactions++
        else 
          setnote(fifth.selectednote, e.offsetY)
          fifth.noteactions++
        if fifth.noteactions > 5 and fifth.chordactions > 5
          ui.end() 
      
      selectaccord = (e) ->
        if fifth.selecting is "note" then $("##{fifth.selectednote}").removeClass "selected"
        # $().removeClass "selected"
        $("##{fifth.selected}").css("opacity", 0.7)
        fifth.selected = e.target.id
        fifth.selecting = "accord"
        # $(e.target).addClass "selected"
        $(e.target).css("opacity", 1)
      
      back.click moveto
      a.click selectaccord
      b.click selectaccord
      c.click selectaccord
      a.css("background-color", palette[1])
      b.css({"background-color": palette[2]})
      c.css("background-color", palette[3])
      [back,a,b,c, ns]
    arpevent : (e) -> $("#f#{e}").animate({top : "-=10px"}, 100, () ->$("#f#{e}").animate({top : ((7 - fifth.noteindexes[e]) * (300/8)) + "px"}, 100))

  fourth = 
    releaselength : 200
    arplength : 4
    selected : 0
    selsize : 3
    noteindexes : [ 0, 4, 10, 6 ]
    # noteindexes : [ 7, 8, 9, 10 ]
    bpm : 140
    notes : () -> 
      r = []
      ii = 0
      for i in fourth.noteindexes
        r.push {f:fourth.scale[i], p : ii}
        ii++
      r
    seltopos : (i) -> 
      switch i
        when 0 then {left:"0px", top:"0px"}
        when 1 then {left:"280px", top:"0px"}
        when 2 then {left:"280px", top:"280px"}
        when 3 then {left:"0px", top:"280px"}
    arpevent : (i) ->
      $("#fr#{i}").animate({height:"140px"}, 100, ()->$("#fr#{i}").animate({height:"150px"}, 100))

    scale : ["E0", "F#0", "G0", "A0", "B0", "C1", "D1", "E1", "F#1", "G1", "A1", "B1", "C2", "D2", "E2", "F#2", "G2", "A2", "B2", "C3", "D3", "E3" ]
    insta : () ->
      a = $("<div>", {id:"fr0", style:"background-color : #{palette[1]}"})
      b = $("<div>", {id:"fr1", style:"background-color : #{palette[2]}"})
      c = $("<div>", {id:"fr2", style:"background-color : #{palette[3]}"})
      d = $("<div>", {id:"fr3", style:"background-color : #000"})
      which = (i) -> "fr#{i}"
      select = (i) ->
        cs = $("#frbuttons").children()
        for c in cs
          $("##{c.id}").css("opacity", 0.6)
        for ii in [0..fourth.selsize]
          c = cs[ (i + ii) % 4 ]
          $("##{c.id}").css("opacity", 1)
      rotateselection = () ->
        fourth.selected = (fourth.selected + 1) % 4
        select fourth.selected
      changeselsize = () ->
        fourth.selsize = (fourth.selsize + 1) % 4
        select fourth.selected
      changenotes = () ->
        for ii in [0..fourth.selsize]
          c = (fourth.selected + ii) % 4
          fourth.noteindexes[c] = (fourth.noteindexes[c] + 1) % fourth.scale.length
        Arp.notes = fourth.notes()
        if Arp.notes[0].f[0] is "E" and Arp.notes[0].f[0] is Arp.notes[1].f[0] and Arp.notes[1].f[0] is Arp.notes[2].f[0] and Arp.notes[2].f[0] is Arp.notes[3].f[0] then (if not finished then load fifth)
      a.click rotateselection
      b.click changeselsize
      c.click changenotes
      d.mouseenter (() -> drone.play())
      d.mousemove((e) -> 
                    center = (p) -> if p < 75 then p / 75 else (150 - p) / 75
                    m = center(e.offsetX) * 0.5 + center(e.offsetY) * 0.5 
                    drone.mul = 0.5 * m
                    drone.freq.value = Notetable[fourth.scale[0]] + m*500
                    
                  )
      d.mousedown (()->drones.freq.value = Notetable[fourth.scale[14]])
      d.mouseup (()->drones.freq.value = Notetable[fourth.scale[0]])
      d.mouseleave (() -> drone.pause())
      con = $("<div>", {id:"frbuttons", html : [a,b,c,d]} )
      back = $("<div>", {id:"back", style : "background-color : #555"})

      [ back, con]

  third =
    releaselength : 300
    arplength : 3
    bpm : 360
    tha : 3
    thb : 5
    thc : 5
    af : (x) ->
      switch x
        when 6, 4, 7, 3 then x - 2
        when 2, 1 then x + 5
        when 5, 0 then x + 3
        when 8 then 0
    bf : (x) -> (x + 1) % 9
    cf : (x) ->
      switch x
        when 3, 4, 5, 6, 7, 8 then x - 3
        when 0, 1 then 7 + x
        when 2 then 6
    scale : ["E0", "F#0", "G0", "A0", "B0", "C1", "D1", "E1", "F#1", "G1", "A1", "B1", "C2"]
    notes : () ->
      [
        {f : third.scale[ (2 - Math.floor(third.tha / 3)) + 0 ], p : third.tha % 3 },
        {f : third.scale[(2 - Math.floor(third.thb / 3)) + 2 ], p : third.thb % 3 },
        {f : third.scale[(2 - Math.floor(third.thc / 3)) + 4], p : third.thc % 3 }
      ]
    tcs : (x) -> { left : "#{(x % 3)*33.3333333}%", top: "#{(x // 3) * 33.3333333}%"}
    move : (e) -> 
      third[e] = third[e.substr(2,1) + "f"] third[e]
      $("##{e}").css (third.tcs third[e])
    insta : () ->
      a = $("<div>", {id:"tha"})
      b = $("<div>", {id:"thb"})
      c = $("<div>", {id:"thc"})
      a.css(third.tcs(third.tha))
      b.css(third.tcs(third.thb))
      c.css(third.tcs(third.thc))
      a.css("background-color",palette[1])
      b.css("background-color",palette[2])
      c.css("background-color",palette[3])
      nextlevel = () ->
        if third.tha is third.thb and third.thb is third.thc
          if not finished then load fourth
          true
        else false
      a.click((e)->
          if not nextlevel() or finished
            third.move "tha"
            third.move "thb"
            third.move "thc"
            Arp.notes = third.notes()
        )
      b.click((e) ->
          third.move "thb"
          third.move "thc"
          Arp.notes = third.notes()
        )
      c.click((e) -> 
              third.move "thc"
              Arp.notes = third.notes()
              )
      back = $("<div>", {id:"back", style : "background-color : #444"})
      [back, a,b,c]

  second = 
    arplength : 2
    releaselength : 222
    bpm : 120
    notes : () -> [{f:"E0", p : 0}, {f:"F#1", p : 1}]
    scale : ["E0", "F#0", "G0", "A0", "B0", "C1", "D1", "E1", "F#1", "G1", "A1", "B1", "C2"]
    update : (p) ->
      $("#left").css({left: "0px", width : p.x+"px"})
      $("#right").css({left : p.x+"px", width : (300 - p.x)+"px"})
      Arp.notes[1].f = second.scale[ Math.floor((p.x / 300)*second.scale.length) ]
      if p.x < 10 
        $("#left").css("opacity", 1.0)
        Arp.notes[0].f = "E1"
      if p.x > 290 and $("#left").css("opacity") is "1" then (if not finished then load third)
    insta : () ->
      left = $("<div>", {id:"left"})
      right = $("<div>", {id:"right"})
      left.css("background-color",palette[1])
      right.css("background-color",palette[3])
      c = $("<div>", {id:"c"})
      c.css {width: "100%", height:"100%", "z-index":100, position:"absolute"}
      c.mousemove((e)-> second.update({x:e.offsetX, y:e.offsetY}) )
      [left,right,c]

  first = 
    arplength : 1
    releaselength : 144
    bpm : 666
    notes : () -> []
    scale : ["E0","G0", "B0", "E1"]
    i : 0
    pt : 0
    hover : false
    arpevent : (e) ->
      if first.hover
        $("#fst").css("background-color", palette[first.pt])
        first.pt = (first.pt + 1) % palette.length
    insta : () ->
      first.i = 0
      first.pt = 0
      first.unmuted = false

      button = $("<div>", {id:"fst"})
      button.css {width : "20px", height : "20px", left:"140px", top:"140px", "background-color":palette[3]}
      button.click((e)->
        if first.i < 3
          $("#fst").animate({width : "+=95px", height : "+=95px", left:"-=47px", top:"-=47px"}, 200)
          first.i++
          Arp.notes[0].f = first.scale[first.i]
        if $("#fst").width() >= 300 
          if not finished then load second
        )
      button.mouseenter((e)->
                          Arp.notes = [{f:first.scale[first.i], p : 0}]
                          first.hover = true
                          )
      button.mouseleave((e)->
        Arp.notes = []
        $("#fst").css("background-color", palette[2])
        if $(e.target).width() > 70
          first.i--
          $("#fst").animate({width : "-=95px", height : "-=95px", left:"+=47px", top:"+=47px"}, 200)
        first.hover = false
        )
      [button]

  levels = [first, second, third, fourth, fifth]
  loadlevel = (i) -> 
    console.log i
    load (levels[i - 1])
  help = () -> 
    ui.end()
    "no problem"
  console.log "give up? call help() to unlock all levels"
  $("#help").click((e)-> if confirm("unlock all 5 levels?") then help() else false)



  load(first)


muted = true
$("#start").click(() ->
  if muted
    TTT()
    unmuted = true
    init()
)
# ui.end()
